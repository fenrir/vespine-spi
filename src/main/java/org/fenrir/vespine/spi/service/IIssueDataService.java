package org.fenrir.vespine.spi.service;

import java.util.Date;
import java.util.List;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.dto.ISeverityDTO;
import org.fenrir.vespine.spi.dto.IStatusDTO;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO v1.0 Documentació
 * @author Anatonio Archilla Nava
 * @version v0.1.20140103
 */
public interface IIssueDataService 
{
	public String getProvider();
	
	public List<IProjectDTO> findAvailableProjects() throws BusinessException;
    public List<IStatusDTO> findAllStatus() throws BusinessException;
    public List<ISeverityDTO> findAllSeverities() throws BusinessException;
    public List<String> findProjectCategories(Long projectId) throws BusinessException;
    public IIssueDTO findIssue(Long issueId) throws BusinessException;        
    public List<IIssueDTO> findProjectIssues(Long projectId, int pageNumber, int pageSize) throws BusinessException;
    public List<IIssueDTO> findProjectIssues(Long projectId, Date limitDate, int pageNumber, int pageSize) throws BusinessException;
}
