package org.fenrir.vespine.spi;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class ApplicationContextHandler 
{
	private static ApplicationContextHandler instance;
	
	private IApplicationContext applicationContext;
	
	private ApplicationContextHandler()
	{
		
	}
	
	public static ApplicationContextHandler getInstance()
	{
		if(instance==null){
			instance = new ApplicationContextHandler();
		}
		
		return instance;
	}
	
	public IApplicationContext getApplicationContext()
	{
		return applicationContext;
	}
	
	public void setApplicationContext(IApplicationContext applicationContext)
	{
		this.applicationContext = applicationContext;
	}
}
