package org.fenrir.vespine.spi.dto;

import java.util.Date;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140110
 */
public interface IUserDTO 
{
	public String getProvider();
	
	public String getUserId();
	public String getUsername();
	public String getCompleteName();
	public Boolean isActive();
	public Date getLastUpdated();
}
