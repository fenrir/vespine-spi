package org.fenrir.vespine.spi.dto;

import java.util.Date;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140111
 */
public interface IProviderElementDTO 
{
	public String getType();
	
	public String getProviderElementId();
	public String getName();
	public String getProvider();
	public String getProviderId();
	public Date getLastUpdated();
}
