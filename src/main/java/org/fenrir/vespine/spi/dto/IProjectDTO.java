package org.fenrir.vespine.spi.dto;

import java.util.Date;
import java.util.List;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140418
 */
public interface IProjectDTO 
{
	public String getProvider();
	
    public String getProjectId();
    public String getName();
    public String getAbbreviation();
    public IProjectDTO getParentProject();
    public List<IProjectDTO> getSubprojects();
    public Date getLastUpdated();
}
