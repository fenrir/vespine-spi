package org.fenrir.vespine.spi.dto;

import java.util.Date;
import java.util.List;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140613
 */
public interface IIssueDTO 
{
	public String getProvider();
	
	public String getIssueId();
	public String getVisibleId();
    public String getSummary();
    public String getDescription();
    public IStatusDTO getStatus();
    public IProjectDTO getProject();
    public ISeverityDTO getSeverity();
    public ICategoryDTO getCategory();
    public IUserDTO getReporter();
    public IUserDTO getAssignedUser();
    public Date getSendDate();
    public Date getResolutionDate();
    public Date getModifiedDate();
    public Boolean isSla();
    public Date getSlaDate();
    public Integer getEstimatedTime();
    public String getEstimatedTimeFormated();
    public List<ITagDTO> getTags();
    public List<IIssueNoteDTO> getNotes();
    public Date getLastUpdated();
}
