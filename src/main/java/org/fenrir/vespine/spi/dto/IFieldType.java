package org.fenrir.vespine.spi.dto;

import java.util.Map;
import org.fenrir.vespine.spi.util.IAttributeConverter;

public interface IFieldType 
{
	public static final String TYPE_STRING = "STRING";
	public static final String TYPE_NUMERIC = "NUMERIC";
	public static final String TYPE_BOOLEAN = "BOOLEAN";
	public static final String TYPE_DATE = "DATE";
	public static final String TYPE_TIMESTAMP = "TIMESTAMP";
	public static final String TYPE_DICTIONARY = "DICTIONARY";
	
	public String getType();
	public Class<? extends IAttributeConverter<?, ?>> getConverter();
	public Map<String, String> getConverterParameters();
	public boolean needDataProviderId();
}
