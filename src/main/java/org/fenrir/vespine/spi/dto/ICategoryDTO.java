package org.fenrir.vespine.spi.dto;

import java.util.Date;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140111
 */
public interface ICategoryDTO 
{
	public String getProvider();
	
	public String getCategoryId();
	public String getName();
	public IProjectDTO getProject();
	public Date getLastUpdated();
}
