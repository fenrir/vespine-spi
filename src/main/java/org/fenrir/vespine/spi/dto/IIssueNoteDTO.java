package org.fenrir.vespine.spi.dto;

import java.util.Date;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140510
 */
public interface IIssueNoteDTO 
{
	public String getProvider();
	
    public String getNoteId();
    public IIssueDTO getIssue();
    public String getReporter();
    public String getText();    
    public Date getSendDate();
    public Date getLastEditionDate();
}
