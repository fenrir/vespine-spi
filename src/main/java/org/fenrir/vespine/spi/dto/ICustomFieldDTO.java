package org.fenrir.vespine.spi.dto;

import java.util.Date;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140330
 */
public interface ICustomFieldDTO 
{
	public String getProvider();
	
	public String getFieldId();
	public IFieldType getFieldType();
	public String getName();
	public boolean isMandatory();
	public Date getLastUpdated();
}
