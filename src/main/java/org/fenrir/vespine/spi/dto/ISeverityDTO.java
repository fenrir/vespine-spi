package org.fenrir.vespine.spi.dto;

import java.util.Date;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140111
 */
public interface ISeverityDTO 
{
	public String getProvider();
	
    public String getSeverityId();
    public String getName();
    public String getSlaTerm();
    public Date getLastUpdated();
}
