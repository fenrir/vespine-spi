package org.fenrir.vespine.spi.dto;

import java.util.Date;

public interface ICustomValueDTO 
{
	public String getProvider();
	
	public String getValueId();
	public ICustomFieldDTO getField();
	public IIssueDTO getIssue();
	public String getValue();
	public Date getLastUpdated();
}
