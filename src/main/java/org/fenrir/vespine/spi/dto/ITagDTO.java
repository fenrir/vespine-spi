package org.fenrir.vespine.spi.dto;

import java.util.Date;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140107
 */
public interface ITagDTO 
{
	public String getTagId();
	public String getName();
	public Boolean isPreferred();
	public Date getCreationDate();
	public Date getLastUpdated();
}
