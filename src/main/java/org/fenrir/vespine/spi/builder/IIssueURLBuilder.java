package org.fenrir.vespine.spi.builder;

/**
 * TODO v1.0 Documentació
 * @author Anatonio Archilla Nava
 * @version v0.1.20140103
 */
public interface IIssueURLBuilder 
{
	public String getProvider();
	
	public String buildIssueURL(String issueId);
}
