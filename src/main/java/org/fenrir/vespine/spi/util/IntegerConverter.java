package org.fenrir.vespine.spi.util;


/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class IntegerConverter implements IAttributeConverter<Integer, String> 
{
	@Override
	public void setParameter(String name, String value)
	{
		// No aplica
	}
	
	@Override
	public String convertToDatabaseColumn(Integer attributeValue)
	{
		return attributeValue==null ? null : attributeValue.toString();
	}
	
	@Override
	public Integer convertToEntityAttribute(String columnValue)
	{
		return columnValue==null ? null : Integer.valueOf(columnValue.toString());
	}
}
