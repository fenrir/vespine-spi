package org.fenrir.vespine.spi.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class DateConverter implements IAttributeConverter<Date, String> 
{
	public static final String PARAM_NAME_PATTERN = "pattern";
	
	public static final String DATE_FORMAT = "dd/MM/yyyy";
	public static final String TIMESTAMP_FORMAT = "dd/MM/yyyy HH:mm:ss";
	
	private Map<String, String> parameters = new HashMap<String, String>();
	
	public void setParameter(String name, String value)
	{
		parameters.put(name, value);
	}
	
	public String convertToDatabaseColumn(Date attributeValue)
	{
		String pattern = StringUtils.defaultString(parameters.get(PARAM_NAME_PATTERN), DATE_FORMAT);
		return new SimpleDateFormat(pattern).format(attributeValue);
	}
	
	public Date convertToEntityAttribute(String columnValue)
	{
		try{
			String pattern = StringUtils.defaultString(parameters.get(PARAM_NAME_PATTERN), DATE_FORMAT);
			return new SimpleDateFormat(pattern).parse(columnValue);
		}
		catch(ParseException e){
			throw new RuntimeException("Error transformant el valor " + columnValue, e);
		}
	}
}