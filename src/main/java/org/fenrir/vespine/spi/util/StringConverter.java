package org.fenrir.vespine.spi.util;


/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class StringConverter implements IAttributeConverter<String, String> 
{
	@Override
	public void setParameter(String name, String value)
	{
		// No aplica
	}
	
	public String convertToDatabaseColumn(String attributeValue)
	{
		return attributeValue;
	}
	
	public String convertToEntityAttribute(String columnValue)
	{
		return columnValue;
	}
}
