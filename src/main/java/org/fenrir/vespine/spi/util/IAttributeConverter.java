package org.fenrir.vespine.spi.util;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140513
 */
public interface IAttributeConverter <T, U> 
{
	public void setParameter(String name, String value);
	
	public U convertToDatabaseColumn(T attributeValue);
	public T convertToEntityAttribute(U columnValue);
}
