package org.fenrir.vespine.spi.util;


/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class BooleanConverter implements IAttributeConverter<Boolean, String> 
{
	@Override
	public void setParameter(String name, String value)
	{
		// No aplica
	}
	
	@Override
	public String convertToDatabaseColumn(Boolean attributeValue)
	{
		return attributeValue==null ? null : attributeValue.toString();
	}
	
	@Override
	public Boolean convertToEntityAttribute(String columnValue)
	{
		return columnValue==null ? null : Boolean.valueOf(columnValue.toString());
	}
}
