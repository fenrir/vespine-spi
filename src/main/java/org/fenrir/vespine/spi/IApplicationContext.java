package org.fenrir.vespine.spi;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface IApplicationContext 
{
	public Object getRegisteredComponent(Class<?> type);
	public Object getRegisteredComponent(Class<?> type, String name);
	public void injectMembers(Object object);
}
